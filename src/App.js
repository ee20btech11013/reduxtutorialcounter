import logo from "./logo.svg";
import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { actions } from "./store";

function App() {
  const count = useSelector((state) => state.count);
  const dispatch = useDispatch();
  const Increment = () => {
    dispatch(actions.increment());
  };
  const Decrement = () => {
    dispatch(actions.decrement());
  };
  const IncrementBy10 = () => {
    dispatch(actions.addBy(10));
  };
  return (
    <div className="App">
      <h1>{count}</h1>
      <button onClick={Increment}>Increment</button>
      <button onClick={Decrement}>Decrement</button>
      <button onClick={IncrementBy10}>Increment By 10</button>
    </div>
  );
}

export default App;
