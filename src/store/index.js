import { configureStore, createSlice } from "@reduxjs/toolkit";

const counterslice = createSlice({
  name: "counter",
  initialState: { count: 0 },
  reducers: {
    increment(state, action) {
      state.count++;
    },
    decrement(state, action) {
      state.count--;
    },
    addBy(state, action) {
      state.count += action.payload;
    },
  },
});

const store = configureStore({
  reducer: counterslice.reducer,
});

export const actions = counterslice.actions;

export default store;
